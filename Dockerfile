FROM debian:stable-slim

ENV DEBIAN_FRONTEND noninteractive

# setup workdir
RUN mkdir -p /usr/local/workdir

# install git and slim down image
RUN apt-get -y update && apt-get -y install git curl && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/man/?? /usr/share/man/??_*
